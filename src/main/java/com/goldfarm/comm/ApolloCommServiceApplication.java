package com.goldfarm.comm;

import com.goldfarm.comm.dao.models.system.SystemConfigDTO;
import com.goldfarm.comm.factories.FactoryProvider;
import com.goldfarm.comm.representation.config.MongoDbConfig;
import com.goldfarm.comm.resources.MessagingResource;
import com.goldfarm.comm.utils.ApolloConfigUtils;
import com.goldfarm.comm.utils.MongoClientUtils;
import com.goldfarm.comm.utils.OkHttpClientUtils;
import com.goldfarm.comm.validator.ValidateRequestServletFilter;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import io.dropwizard.Application;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import org.glassfish.jersey.filter.LoggingFilter;
import org.hibernate.SessionFactory;

import javax.servlet.DispatcherType;
import javax.ws.rs.client.Client;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.logging.Logger;

public class ApolloCommServiceApplication extends Application<ApolloCommServiceConfiguration> {

    public static void main(final String[] args) throws Exception {
        new ApolloCommServiceApplication().run(args);
    }

    @Override
    public String getName() {
        return "ApolloCommService";
    }

    @Override
    public void initialize(final Bootstrap<ApolloCommServiceConfiguration> bootstrap) {
        bootstrap.addBundle(hibernate);
        bootstrap.addBundle(new SwaggerBundle<ApolloCommServiceConfiguration>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(ApolloCommServiceConfiguration configuration) {
                return configuration.swaggerBundleConfiguration;
            }
        });
    }

    private final HibernateBundle<ApolloCommServiceConfiguration> hibernate =
            new HibernateBundle<ApolloCommServiceConfiguration>(SystemConfigDTO.class) {
                @Override
                public DataSourceFactory getDataSourceFactory(ApolloCommServiceConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
            };

    @Override
    public void run(final ApolloCommServiceConfiguration configuration,
                    final Environment environment) {
        final SessionFactory sessionFactory = hibernate.getSessionFactory();
        FactoryProvider.getInstance().setSessionFactory(sessionFactory);

        //Add Managed for managing the Mongo instance
        MongoDbConfig mongoDbConfig = configuration.getMongoDbConfig();

        // Manage the mongo db connection...
        List<ServerAddress> seeds = new ArrayList<ServerAddress>();
        seeds.add(new ServerAddress(mongoDbConfig.getMongoHost(), mongoDbConfig.getMongoPort()));
        List<MongoCredential> credentials = new ArrayList<MongoCredential>();
        credentials.add(
                MongoCredential.createCredential(
                        mongoDbConfig.getUserName(),
                        mongoDbConfig.getAuthDb(),
                        mongoDbConfig.getPassword().toCharArray()
                )
        );

        //---------- Connecting DataBase -------------------------//
        MongoClient mongoClient = new MongoClient(seeds, credentials);

        MongoDatabase mongoDatabase = mongoClient.getDatabase(mongoDbConfig.getMongoDb());

        MongoClientUtils mongoClientUtils = MongoClientUtils.getInstance();
        mongoClientUtils.setMongoClient(mongoClient);

        ApolloConfigUtils apolloConfigUtils = ApolloConfigUtils.getInstance();
        apolloConfigUtils.setConfig(configuration);
        OkHttpClientUtils okHttpClientUtils = OkHttpClientUtils.getOkHttpClientInstance();
        environment.jersey().register(new MessagingResource());
        if (configuration.isHttpDebuggingEnabled()) {
            // Logging outbound request/response
            Client jersyClient = new JerseyClientBuilder(environment)
                    .build("comm_client")
                    .register(new LoggingFilter(Logger.getLogger("OutboundRequestResponse"), true));
            // Logging inbound request/response
            environment.jersey().register(new LoggingFilter(Logger.getLogger("InboundRequestResponse"), true));
        }
        environment.servlets().addFilter("ValidateRequestServletFilter", new ValidateRequestServletFilter(configuration))
                .addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/comm/*");

    }

}
