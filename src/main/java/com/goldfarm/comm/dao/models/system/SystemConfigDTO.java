package com.goldfarm.comm.dao.models.system;

import io.dropwizard.jackson.JsonSnakeCase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;

@Getter
@Setter
@Entity
@Table(name = "gdf_hb_system_config", schema = "gdf_hb_apollo", catalog = "")
@JsonSnakeCase
public class SystemConfigDTO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private BigInteger id;

    @Column(name = "config_name")
    private String configName;

    @Column(name = "config_type")
    private BigInteger configType;

    @Column(name = "config_value")
    private String configValue;

    @Column(name = "desc")
    private String desc;

    @Column(name = "status_id")
    private BigInteger statusId;

    @Column(name = "params")
    private String params;


}
