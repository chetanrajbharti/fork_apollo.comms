package com.goldfarm.comm.constant;

public enum PushNotificationType {
    SMALL_NOTIFICAION("small_notification"),
    BIG_NOTIFICAION("big_notification");

    final String notificationType;

    public String getNotificationType() {
        return notificationType;
    }

    PushNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }
}
