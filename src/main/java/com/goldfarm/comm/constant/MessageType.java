package com.goldfarm.comm.constant;

public enum MessageType {
    SMS("sms"),
    EMAIL("email"),
    PUSH_NOTIFICATION("push_notification");

    final String messageType;

    public String getMessageType() {
        return messageType;
    }

    MessageType(String messageType) {
        this.messageType = messageType;
    }
}
