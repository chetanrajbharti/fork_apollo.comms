package com.goldfarm.comm.constant;

import java.util.HashMap;

/**
 * Created by chetan on 26/6/17.
 */
public class ResponseCodeValueMapper {
    public static final HashMap<Integer, String> RESP_CODE_VALUE_MAP;

    /*Response message as per response code*/
    public static final String NO_RESULT_FOUND = "No result found";

    /*Response codes*/
    public static final Integer SUCCESS = 1;
    public static final Integer FAILURE = 0;
    public static final Integer NO_CONTENT = 204;

    public static final Integer ADDRESS_NOT_ADDED = 403;
    public static final Integer SUCCESS_CODE = 200;

    static {
        RESP_CODE_VALUE_MAP = new HashMap<Integer, String>();

        RESP_CODE_VALUE_MAP.put(SUCCESS,"Success");
        RESP_CODE_VALUE_MAP.put(SUCCESS_CODE,"Result found");
        RESP_CODE_VALUE_MAP.put(NO_CONTENT,NO_RESULT_FOUND);
    }
}
