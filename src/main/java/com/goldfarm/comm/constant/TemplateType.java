package com.goldfarm.comm.constant;

public enum TemplateType {
    SMS_NEW_ORDER("sms_new_order"),
    SMS_CONFIRMED_ORDER("sms_confirmed_order"),
    SMS_CANCELED_ORDER("sms_canceled_order"),
    EMAIL("email"),
    STANDARD_PUSH_NOTIFICATION("small_push_notification"),
    BIG_PUSH_NOTIFICATION("big_push_notification");

    final String templateType;

    public String getTemplateType() {
        return templateType;
    }

    TemplateType(String templateType) {
        this.templateType = templateType;
    }
}
