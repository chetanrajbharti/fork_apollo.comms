package com.goldfarm.comm.command;

import com.goldfarm.comm.messaging.ApolloMessaging;
import com.goldfarm.comm.messaging.MessageDataObject;
import com.goldfarm.comm.messaging.utils.ApolloMessagingImpl;
import com.goldfarm.comm.representation.domainObject.modal.RecipientData;
import com.goldfarm.comm.representation.modal.UserDTO;
import com.goldfarm.comm.representation.request.NotificationRequest;
import com.goldfarm.comm.representation.response.NotificationResponse;

import javax.ws.rs.core.Response;

/**
 * Created by sam on 24/8/16.
 */
public class ProducerCommand implements Command {
    private NotificationRequest notificationRequest;

    public ProducerCommand(NotificationRequest notificationRequest) {
        this.notificationRequest = notificationRequest;
    }

    @Override
    public Response run() {
        UserDTO userDTO = new UserDTO();
        userDTO.setPhone(notificationRequest.getPhoneNumber());
        RecipientData recipientData = new RecipientData(userDTO, notificationRequest.getGfCode(), notificationRequest.getRoleId());
        MessageDataObject messageDataObject = MessageDataObject.messageBuilder()
                .messageTypeList(notificationRequest.getMessageTypeList())
                .textMessage(notificationRequest.getTextMessage())
                .pushNotificationImageUrl(notificationRequest.getPushNotificationImageUrl())
                .pushNotificationType(notificationRequest.getPushNotificationType())
                .pushNotificationMessage(notificationRequest.getPushNotificationMessage())
                .pushNotificationTitle(notificationRequest.getPushNotificationTitle())
                .emailTemplateType(notificationRequest.getEmailTemplateType())
                .emailSubject(notificationRequest.getEmailSubject())
                .emailMessageBody(notificationRequest.getEmailMessageBody())
                .smsTemplateType(notificationRequest.getSmsTemplateType())
                .build();
        ApolloMessaging apolloMessaging = new ApolloMessagingImpl();
        NotificationResponse notificationResponse = apolloMessaging.sendMessage(recipientData, messageDataObject);
        return Response.status(Response.Status.OK).entity(notificationResponse).build();
    }
}
