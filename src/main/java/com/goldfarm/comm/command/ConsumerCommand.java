package com.goldfarm.comm.command;

import javax.ws.rs.core.Response;

/**
 * Created by sam on 24/8/16.
 */
public class ConsumerCommand implements Command {
    @Override
    public Response run() {
        return Response.status(Response.Status.OK).build();
    }
}
