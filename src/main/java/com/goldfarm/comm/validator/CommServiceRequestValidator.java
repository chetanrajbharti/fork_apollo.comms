package com.goldfarm.comm.validator;

import com.goldfarm.comm.ApolloCommServiceConfiguration;
import com.goldfarm.comm.constant.Constant;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by umashankar on 4/7/17.
 */
public class CommServiceRequestValidator implements RequestValidator {

    private ApolloCommServiceConfiguration apolloConfiguration;

    public CommServiceRequestValidator(ApolloCommServiceConfiguration apolloConfiguration) {
        this.apolloConfiguration = apolloConfiguration;
    }

    @Override
    public boolean validateRequest(ServletRequest servletRequest) {
        if (servletRequest instanceof HttpServletRequest) {
            String authToken = ((HttpServletRequest) servletRequest).getHeader(Constant.X_SESSION_ID);
            if (authToken == null) {
                return false;
            } else if (authToken.equals(Constant.APOLLO_WEB_SERVER)) {
                return true;
            }
        }
        return false;
    }
}
