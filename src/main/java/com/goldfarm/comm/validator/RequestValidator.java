package com.goldfarm.comm.validator;

import javax.servlet.ServletRequest;

/**
 * Created by umashankar on 4/7/17.
 */
public interface RequestValidator {
    boolean validateRequest(ServletRequest servletRequest);
}
