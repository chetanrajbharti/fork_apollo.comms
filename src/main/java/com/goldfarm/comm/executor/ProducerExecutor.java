package com.goldfarm.comm.executor;


import com.goldfarm.comm.command.Command;

import javax.ws.rs.core.Response;

/**
 * Created by sam on 24/8/16.
 */
public class ProducerExecutor implements Executor {
    @Override
    public Response execute(Command command) {
        return command.run();
    }
}
