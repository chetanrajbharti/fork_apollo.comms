package com.goldfarm.comm.utils;

import com.goldfarm.comm.ApolloCommServiceConfiguration;
import okhttp3.OkHttpClient;

public class OkHttpClientUtils {
    private OkHttpClient client;
    private static OkHttpClientUtils okHttpClientUtilsInstance;

    private OkHttpClientUtils() {
        this.client = new OkHttpClient();
    }

    public static OkHttpClientUtils getOkHttpClientInstance() {
        if (okHttpClientUtilsInstance == null) {
            synchronized (OkHttpClientUtils.class) {
                if (okHttpClientUtilsInstance == null) {
                    okHttpClientUtilsInstance = new OkHttpClientUtils();
                }
            }
        }
        return okHttpClientUtilsInstance;
    }

    public OkHttpClient getClient() {
        return client;
    }
}
