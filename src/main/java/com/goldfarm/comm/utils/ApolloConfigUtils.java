package com.goldfarm.comm.utils;


import com.goldfarm.comm.ApolloCommServiceConfiguration;

public class ApolloConfigUtils {
    ApolloCommServiceConfiguration config;

    public ApolloCommServiceConfiguration getConfig() {
        return config;
    }

    public void setConfig(ApolloCommServiceConfiguration config) {
        this.config = config;
    }

    private static ApolloConfigUtils apolloConfigUtilsInstance;

    private ApolloConfigUtils() {
    }

    public static ApolloConfigUtils getInstance() {
        if (apolloConfigUtilsInstance == null) {
            synchronized (ApolloConfigUtils.class) {
                if (apolloConfigUtilsInstance == null) {
                    apolloConfigUtilsInstance = new ApolloConfigUtils();
                }
            }
        }
        return apolloConfigUtilsInstance;
    }
}
