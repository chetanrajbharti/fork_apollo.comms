package com.goldfarm.comm.utils;

import com.mongodb.MongoClient;

public class MongoClientUtils {
    private static MongoClientUtils mongoClientInstance;
    MongoClient mongoClient;

    public MongoClient getMongoClient() {
        return mongoClient;
    }

    public void setMongoClient(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    private MongoClientUtils() {
    }

    public static MongoClientUtils getInstance() {
        if (mongoClientInstance == null) {
            synchronized (MongoClientUtils.class) {
                if (mongoClientInstance == null) {
                    mongoClientInstance = new MongoClientUtils();
                }
            }
        }
        return mongoClientInstance;
    }
}
