package com.goldfarm.comm.messaging;

import com.goldfarm.comm.constant.MessageType;
import com.goldfarm.comm.constant.PushNotificationType;
import com.goldfarm.comm.constant.TemplateType;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@Builder
public class MessageDataObject {
    private List<MessageType> messageTypeList;
    private PushNotificationType pushNotificationType;
    private String pushNotificationMessage;
    private String pushNotificationTitle;
    private String pushNotificationImageUrl;
    private String textMessage;
    private String emailMessageBody;
    private String emailSubject;
    private TemplateType smsTemplateType;
    private TemplateType emailTemplateType;

    @Builder(builderMethodName = "messageBuilder")
    public MessageDataObject(List<MessageType> messageTypeList, PushNotificationType pushNotificationType, String pushNotificationMessage, String pushNotificationTitle, String pushNotificationImageUrl, String textMessage, String emailMessageBody, String emailSubject, TemplateType smsTemplateType, TemplateType emailTemplateType) {
        this.messageTypeList = messageTypeList;
        this.pushNotificationType = pushNotificationType;
        this.pushNotificationMessage = pushNotificationMessage;
        this.pushNotificationTitle = pushNotificationTitle;
        this.pushNotificationImageUrl = pushNotificationImageUrl;
        this.textMessage = textMessage;
        this.emailMessageBody = emailMessageBody;
        this.emailSubject = emailSubject;
        this.smsTemplateType = smsTemplateType;
        this.emailTemplateType = emailTemplateType;
    }
}
