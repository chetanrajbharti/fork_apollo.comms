package com.goldfarm.comm.representation.domainObject.modal;


import com.goldfarm.comm.constant.PushNotificationType;
import com.goldfarm.comm.representation.domainObject.modal.pushNotification.BigPushNotificationPayload;
import com.goldfarm.comm.representation.domainObject.modal.pushNotification.SmallPushNotificationPayload;
import io.dropwizard.jackson.JsonSnakeCase;
import lombok.*;

@Data
@NoArgsConstructor
@JsonSnakeCase
@Builder
@Setter
@Getter
public class Message {
    private String smsBody;
    private String emailBody;
    private SmallPushNotificationPayload smallPushNotificationPayload;
    private BigPushNotificationPayload bigPushNotificationPayload;
    private PushNotificationType pushNotificationType;


    @Builder(builderMethodName = "notificationBuilder")
    public Message(String smsBody, String emailBody, SmallPushNotificationPayload smallPushNotificationPayload, BigPushNotificationPayload bigPushNotificationPayload, PushNotificationType pushNotificationType) {
        this.smsBody = smsBody;
        this.emailBody = emailBody;
        this.smallPushNotificationPayload = smallPushNotificationPayload;
        this.bigPushNotificationPayload = bigPushNotificationPayload;
        this.pushNotificationType = pushNotificationType;
    }
}
