package com.goldfarm.comm.representation.domainObject.modal.pushNotification;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.dropwizard.jackson.JsonSnakeCase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonSnakeCase
@EqualsAndHashCode
public class SmallPushNotificationPayload extends PushNotificationPayload {

    public Notification notification;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonSnakeCase
    @Data
    public static class Notification {
        public String body;
        public String title;
    }
}
