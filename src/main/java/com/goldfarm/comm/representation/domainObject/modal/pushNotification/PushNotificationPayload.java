package com.goldfarm.comm.representation.domainObject.modal.pushNotification;

import io.dropwizard.jackson.JsonSnakeCase;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@JsonSnakeCase
public class PushNotificationPayload {
    public String to;
}
