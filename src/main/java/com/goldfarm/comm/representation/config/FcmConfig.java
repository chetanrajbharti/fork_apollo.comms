package com.goldfarm.comm.representation.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class FcmConfig {
    private String apiKey;
    private String apiUrl;
}
