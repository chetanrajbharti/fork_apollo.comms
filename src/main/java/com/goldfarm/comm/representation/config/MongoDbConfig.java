package com.goldfarm.comm.representation.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "mongohost",
        "mongoport",
        "mongodb",
        "password"
})

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MongoDbConfig {

    @JsonProperty("mongohost")
    public String mongoHost;
    @JsonProperty("mongoport")
    public int mongoPort;
    @JsonProperty("mongodb")
    public String mongoDb;
    @JsonProperty("password")
    public String password;
    @JsonProperty("username")
    public String userName;
    @JsonProperty("authdb")
    public String authDb;

}
