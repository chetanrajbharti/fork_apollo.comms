package com.goldfarm.comm.representation.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.jackson.JsonSnakeCase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by umashankar on 28/10/17.
 */
@JsonSnakeCase
@Getter
@Setter
public class CommonResponse extends BaseResponse {
    public Long id;

    @JsonProperty("gf_code")
    public String gcCode;

    private String itemCode;

    public CommonResponse(Integer status, Integer statusCode, Integer httpStatusCode) {
        super(status, statusCode, httpStatusCode);
    }

    public CommonResponse(Integer status, Integer statusCode, Integer httpStatusCode, Long id) {
        super(status, statusCode, httpStatusCode);
        this.id = id;
    }

    public CommonResponse(Integer status, Integer statusCode, Integer httpStatusCode, Long id, String itemCode) {
        super(status, statusCode, httpStatusCode);
        this.id = id;
        this.itemCode = itemCode;
    }

    public CommonResponse(Integer status, Integer statusCode, Integer httpStatusCode, String gcCode) {
        super(status, statusCode, httpStatusCode);
        this.gcCode = gcCode;
    }
}
