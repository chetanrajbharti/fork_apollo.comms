package com.goldfarm.comm.representation.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NotificationResponse extends BaseResponse {
    private String messageId;

    public NotificationResponse(Integer status, Integer statusCode, Integer httpStatusCode, String messageId) {
        super(status, statusCode, httpStatusCode);
        this.messageId = messageId;
    }
}
